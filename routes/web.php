<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\rumahCont;
use App\Http\Controllers\crudCont;
use App\Http\Controllers\mainCont;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [mainCont::class,'main']);

Route::get('/about', [rumahCont::class,'index']);

Route::get('/edit', [crudCont::class,'edit']);
Route::get('/crud', [crudCont::class,'show']);

Route::post('/simpan', [crudCont::class,'prosesSimpan']);

Route::get('/hapus/{id}',[crudCont::class,'hapus']);

Route::get('/edit/{id}',[crudCont::class,'edit']);

Route::post('/update/{id}',[crudCont::class,'update']);




