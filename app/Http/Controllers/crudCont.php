<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class crudCont extends Controller
{
    public function show (){
        $user = DB::table('user')->get();

        return view('crud',['user'=>$user]);
    }

    public function prosesSimpan(Request $req){
     
        $nama = $req->nama_karyawan;
        $nokaryawan = $req->no_karyawan;
        $notelepon = $req->no_telp_karyawan;
        $jabatan = $req->jabatan_karyawan;
        $divisi = $req->divisi_karyawan;

        DB::table('user')->insert(
            ['nama_karyawan'=>$nama,
            'no_karyawan'=>$nokaryawan, 
            'no_telp_karyawan'=>$notelepon,
            'jabatan_karyawan'=>$jabatan,
            'divisi_karyawan'=>$divisi]
            
        );
        
        return redirect('/crud')->with('status', 'Profile updated!');
    }


    public function hapus($id){
        DB::table('user')->where('id', $id)->delete();

        return redirect('/crud');
    }

    public function edit($id)
    {
        $user = DB::table('user')->where('id', $id)->get();
        return view('edit', ['crud' => $user]);
    }

    public function update (Request $req,Int $id)
    {
        DB::table('user')->where('id', $req->id)->update
        (['nama_karyawan' => $req->nama_karyawan,
        'no_karyawan' => $req->no_karyawan,
        'no_telp_karyawan' => $req->no_telp_karyawan,
        'jabatan_karyawan' => $req->jabatan_karyawan,
        'divisi_karyawan' => $req->divisi_karyawan,]);

        return redirect('/crud');
    }

    
}
