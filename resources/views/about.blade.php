@extends('main')

@section('container')
<br>

    <section class="resume-section" id="about">
        <div class="resume-section-content">
            <br>
            <h1 class="mb-0">
                Galuh
                <span class="text-primary">Indah</span>
            </h1>
            <div class="subheading mb-5">
                Kabupaten Buleleng,Kecamatan Sukasada,Desa Panji-085656461451
                <br>
                <a href="mailto:name@email.com" style="color:blue">galuh.indah@undiksha.ac.id</a>
            </div>
            <p class="lead mb-5">Seorang mahasiswa di Universitas Pendidikan Ganesha Fakultas Teknik dan Kejuruan-Jurusan Teknik Informatika-Prodi Sistem Informasi</p>
            
        </div>
        
    </section>
    <a href="/" class="btn btn-danger btn-lg pull-right">back</a>
<br>

@endsection