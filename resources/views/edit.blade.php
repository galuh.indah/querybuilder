@extends('layouts.main')

@section('container')

<section class="resume-section" id="crud">
@foreach ($user as $kr)
    <form action="/update/{{ $kr->id }}" method="POST">
        {{ csrf_field() }}
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Nama</label>
          <input type="name" class="form-control" name="nama_karyawan" value="{{ $kr->nama_karyawan }}">
        </div>
        <div class="mb-3">
          <label for="exampleInputNumber" class="form-label">No Karyawan</label>
          <input type="number" class="form-control" name="no_karyawan" value="{{ $kr->no_karyawan }}">
        </div>
        <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">No telepon</label>
            <input type="number" class="form-control" name="no_telp_karyawan" value="{{ $kr->no_telp_karyawan }}">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Jabatan</label>
            <input type="Name" class="form-control" name="jabatan_karyawan" value="{{ $kr->jabatan_karyawan }}">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Divisi</label>
            <input type="Name" class="form-control" name="divisi_karyawan" value="{{ $kr->divisi_karyawan }}">
          </div>
        <button type="submit" class="btn btn-success">SAVE</button>
      </form>
      @endforeach
</section>